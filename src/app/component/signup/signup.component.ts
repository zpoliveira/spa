import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from "@angular/forms";
import { AuthService } from "../../auth/auth.service";
import { coerceBooleanProperty } from "@angular/cdk/coercion";
import { RegistarJogadorDto } from "../../dto/registarJogadorDto";
import { SignupService } from "../../services/jogador/signup.service";
import { MatChipInputEvent } from "@angular/material/chips";
import { COMMA, ENTER, SPACE } from "@angular/cdk/keycodes";


@Component({
  selector: 'signup',
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  registarJogadorForm: FormGroup;
  jogador: RegistarJogadorDto;
  isLoading = false;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  private stateChanges: any;

  @Input()
  get required() {
    return this._required;
  }
  set required(req) {
    this._required = coerceBooleanProperty(req);
    this.stateChanges.next();
  }
  private _required = false;

  constructor(public authService: AuthService, private registarJogadorService: SignupService) { }

  @ViewChild('chipListRegistarJogador', { static: true }) chipListRegistarJogador;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];

  onSignup(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    this.initForm();
  }

  initForm() {
    this.registarJogadorForm = new FormGroup({
      nome: new FormControl(this.jogador?.nome, Validators.maxLength(40)),
      email: new FormControl({ value: this.jogador?.email, disabled: true }, Validators.email),
      password: new FormControl(this.jogador?.password, [Validators.minLength(6), Validators.maxLength(16)]),
      humor: new FormControl({ value: this.jogador?.humor, disabled: true }),
      urlFacebook: new FormControl(this.jogador?.urlFacebook, Validators.pattern(new RegExp("^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$"))),
      urlLinkedIn: new FormControl(this.jogador?.urlLinkedIn, Validators.pattern(new RegExp("^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$"))),
      telefone: new FormControl(this.jogador?.telefone, [Validators.minLength(1), Validators.maxLength(14)]),
      dataNascimento: new FormControl(new Date(this.jogador?.dataNascimento), Validators.pattern(new RegExp("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$"))),
      nacionalidade: new FormControl(this.jogador?.nacionalidade, [Validators.minLength(1), Validators.maxLength(40)]),
      cidadeResidencia: new FormControl(this.jogador?.cidadeResidencia, [Validators.minLength(1), Validators.maxLength(40)]),
      descricaoBreve: new FormControl(this.jogador?.descricaoBreve, [Validators.minLength(1), Validators.maxLength(4000)]),
      tagList: new FormControl(this.jogador?.taglist),
      avatar: new FormControl('')
    });
  }

  registarJogadorDto: RegistarJogadorDto;
  save(): void {
    this.registarJogadorDto = this.registarJogadorForm.value;
    this.registarJogadorDto.email = this.jogador.email;
    this.registarJogadorDto.password = this.jogador.password;
    this.registarJogadorDto.telefone = Number(this.jogador.telefone);
    this.registarJogadorDto.urlFacebook = this.jogador.urlFacebook;
    this.registarJogadorDto.urlLinkedIn = this.jogador.urlLinkedIn;
    this.registarJogadorDto.dataNascimento = Number(this.jogador.dataNascimento);
    this.registarJogadorDto.humor = this.jogador.humor;
    this.registarJogadorDto.nacionalidade = this.jogador.nacionalidade;
    this.registarJogadorDto.cidadeResidencia = this.jogador.cidadeResidencia;
    this.registarJogadorDto.descricaoBreve = this.jogador.descricaoBreve;
    this.registarJogadorDto.avatar = this.jogador.avatar;
    this.registarJogadorDto.taglist = this.jogador.taglist;

    this.registarJogadorService.registarJogador(this.registarJogadorDto).subscribe(e => e);

  }


  addTag(event: MatChipInputEvent) {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim() && this.jogador.taglist.indexOf(value.trim()) < 0) {
      this.jogador.taglist.push(value.trim())
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeTag(tag: string): void {
    const index = this.jogador.taglist.indexOf(tag);
    if (index >= 0) {
      this.jogador.taglist.splice(index, 1);
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

}
