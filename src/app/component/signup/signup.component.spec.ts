// @ts-ignore

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SignupComponent } from "./signup.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { AuthService } from "../../auth/auth.service";
import { SignupService } from "../../services/jogador/signup.service";
import { NgForm } from "@angular/forms";
import { COMMA, ENTER, SPACE } from "@angular/cdk/keycodes";

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;

  beforeEach(async () => {
    const authServiceStub = () => ({});
    const signupServiceStub = () => ({
      registarJogador: registarJogadorDto => ({ subscribe: f => f({}) })
    });
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [SignupComponent],
      providers: [
        { provide: AuthService, useFactory: authServiceStub },
        { provide: SignupService, useFactory: signupServiceStub }
      ]
    });
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("can load instance", () => {
    expect(component).toBeTruthy();
  });

  it(`isLoading has default value`, () => {
    expect(component.isLoading).toEqual(false);
  });

  it(`visible has default value`, () => {
    expect(component.visible).toEqual(true);
  });

  it(`selectable has default value`, () => {
    expect(component.selectable).toEqual(true);
  });

  it(`removable has default value`, () => {
    expect(component.removable).toEqual(true);
  });

  it(`addOnBlur has default value`, () => {
    expect(component.addOnBlur).toEqual(true);
  });

  it(`separatorKeysCodes has default value`, () => {
    expect(component.separatorKeysCodes).toEqual([ENTER, COMMA, SPACE]);
  });

  describe("onSignup", () => {
    it("makes expected calls", () => {
      const ngFormStub: NgForm = <any>{};
      spyOn(component, "initForm").and.callThrough();
      component.onSignup(ngFormStub);
      expect(component.initForm).toHaveBeenCalled();
    });
  });

  describe("save", () => {
    it("makes expected calls", () => {
      const signupServiceStub: SignupService = fixture.debugElement.injector.get(
        SignupService
      );
      spyOn(signupServiceStub, "registarJogador").and.callThrough();
      component.save();
      expect(signupServiceStub.registarJogador).toHaveBeenCalled();
    });
  });

  describe("ngOnInit", () => {
    it("makes expected calls", () => {
      spyOn(component, "initForm").and.callThrough();
      component.ngOnInit();
      expect(component.initForm).toHaveBeenCalled();
    });
  });

  describe('SignupComponent', () => {
    let component: SignupComponent;
    let fixture: ComponentFixture<SignupComponent>;

    beforeEach(async () => {
      await TestBed.configureTestingModule({
        declarations: [SignupComponent]
      })
        .compileComponents();
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(SignupComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });
});
