import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AceitarLigacaoDto } from 'src/app/dto/aceitarLigacaoDto';
import { EditarRelacionamentoDto } from 'src/app/dto/Relacionamento/editarRelacionamentoDto';

import { LigacaoPendenteDto } from 'src/app/model/ligacaoPendente';
import { Relacionamento } from 'src/app/model/relacionamento';
import { PedidosLigacaoService } from 'src/app/services/pedidos-ligacao/pedidos-ligacao.service';
import { EditarRelacionamentoComponent } from '../editar-relacionamento/editar-relacionamento.component';



@Component({
  selector: 'app-lista-pedidos-pendentes',
  templateUrl: './lista-pedidos-pendentes.component.html',
  styleUrls: ['./lista-pedidos-pendentes.component.scss']
})
export class ListaPedidosPendentesComponent implements OnInit {

  ligacoesPendentes: LigacaoPendenteDto[] | any[] = [];

  private userEmail: string = "1181846@isep.ipp.pt";

  constructor(
    private pedidosLigacaoService: PedidosLigacaoService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    console.log("Init");

    this.getLigacoesPendentes();
  }

  aceitarLigacao(ligacao: LigacaoPendenteDto) {

    let data: Relacionamento = {
      emailDe: this.userEmail,
      emailPara: ligacao.emailJogador,
      forcaLigacao: ligacao.forcaLigacao,
      avatar: "",
      tags: [],
      forcaRelacao: 0,
      nome: ""
    }

    this.acceptLigacao(data, ligacao.idLigacao)
  }

  private getLigacoesPendentes() {
    this.pedidosLigacaoService.getLigacoesPendentes(this.userEmail)
      .subscribe(x => { this.ligacoesPendentes = x; });
  }

  private acceptLigacao(relacionamento: Relacionamento, id: string) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    let editRelacionamento: EditarRelacionamentoDto = {
      Nome: relacionamento.nome,
      EmailJogadorPara: relacionamento.emailPara,
      EmailJogadorDe: relacionamento.emailDe,
      Forca: relacionamento.forcaLigacao,
      LstTags: Object.assign([], relacionamento.tags)
    }

    dialogConfig.data = editRelacionamento

    const dialogRef = this.dialog.open(EditarRelacionamentoComponent,
      dialogConfig);

    dialogRef.componentInstance.submitClicked
      .subscribe(result => {
        console.log('Got the data!', result);
        this.send(result, id);
        dialogRef.close();
      });
  }


  private send(relacionamento: EditarRelacionamentoDto, id: string) {

    let update: AceitarLigacaoDto = {
      idIntroducao: id,
      emailJogador: relacionamento.EmailJogadorPara,
      aceitar: true,
      forcaLigacao: relacionamento.Forca,
      tags: [...relacionamento.LstTags]
    }
    console.log(update);

    this.pedidosLigacaoService.acceptLigacao(update)
      .subscribe(x => {
        console.log(x);
        this.getLigacoesPendentes();
      });
  }
}
