import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPedidosPendentesComponent } from './lista-pedidos-pendentes.component';

describe('ListaPedidosPendentesComponent', () => {
  let component: ListaPedidosPendentesComponent;
  let fixture: ComponentFixture<ListaPedidosPendentesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListaPedidosPendentesComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPedidosPendentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
