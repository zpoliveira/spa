import { EditarJogadorDto } from '../../dto/editarJogadorDto';
import { Component, ElementRef, OnChanges, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common'
import { EditarJogadorService } from '../../services/editar-jogador/editar-jogador.service';
import { Jogador } from '../../model/jogador';


@Component({
  selector: 'app-editarJogador',
  templateUrl: './editarJogador.component.html',
  styleUrls: ['./editarJogador.component.scss']
})
export class EditarJogadorComponent implements OnInit {
  editarJogadorForm: FormGroup;
  listaJogadores?: Jogador[] = [];
  jogador: Jogador;

  constructor(private location: Location, private jogadorService: EditarJogadorService) { }

  ngOnInit(): void {
    this.getJogadorByEmailAsync('1161335@isep.ipp.pt');
  }

  getJogadorByEmailAsync(email: string) {
    this.jogadorService.getJogadorByEmailAsync(email)
      .subscribe(jogadores => {
        this.jogador = { ...jogadores[0] }
        this.initForm();

      });
  }

  initForm() {

    this.editarJogadorForm = new FormGroup({
      nome: new FormControl(this.jogador?.nome, Validators.maxLength(40)),
      email: new FormControl({ value: this.jogador?.email, disabled: true }, Validators.email),
      password: new FormControl(this.jogador?.password, [Validators.minLength(6), Validators.maxLength(16)]),
      humor: new FormControl({ value: this.jogador?.humor, disabled: true }),
      urlFacebook: new FormControl(this.jogador?.urlFacebook, Validators.pattern(new RegExp("^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$"))),
      urlLinkedIn: new FormControl(this.jogador?.urlLinkedIn, Validators.pattern(new RegExp("^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$"))),
      telefone: new FormControl(this.jogador?.telefone, [Validators.minLength(1), Validators.maxLength(14)]),
      dataNascimento: new FormControl(new Date(this.jogador?.dataNascimento), Validators.pattern(new RegExp("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$"))),
      nacionalidade: new FormControl(this.jogador?.nacionalidade, [Validators.minLength(1), Validators.maxLength(40)]),
      cidadeResidencia: new FormControl(this.jogador?.cidadeResidencia, [Validators.minLength(1), Validators.maxLength(40)]),
      descricaoBreve: new FormControl(this.jogador?.descricaoBreve, [Validators.minLength(1), Validators.maxLength(4000)]),
      avatar: new FormControl('')
    });
  }

  @ViewChild('uploadControl') uploadControl: ElementRef;
  uploadFileName = 'Choose File';

  onFileChange(e: any) {

    if (e.target.files && e.target.files[0]) {

      this.uploadFileName = '';
      Array.from(e.target.files).forEach((file: File) => {
        this.uploadFileName += file.name + ',';
      });

      const fileReader = new FileReader();
      fileReader.onload = (e: any) => {
        const img = new Image();
        img.src = e.target.result;
        img.onload = res => {

          const imgBase64Path = e.target.result;

        };
      };
      fileReader.readAsDataURL(e.target.files[0]);

      this.uploadControl.nativeElement.value = "";
    } else {
      this.uploadFileName = 'Choose File';
    }
  }

  back(): void {
    this.location.back()
  }

  jogadorDto: EditarJogadorDto = new EditarJogadorDto();
  save(): void {
    this.jogadorDto = this.editarJogadorForm.value;
    this.jogadorDto.email = this.jogador.email;
    this.jogadorDto.humor = this.jogador.humor;
    this.jogadorDto.tagList = ["tag"];

    this.jogadorService.updateUtilizador(this.jogadorDto).subscribe(e => e);

  }
}



