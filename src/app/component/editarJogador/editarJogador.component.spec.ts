import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Location } from '@angular/common';
import { EditarJogadorService } from '../../services/editar-jogador/editar-jogador.service';
import { EditarJogadorComponent } from './editarJogador.component';

describe('EditarJogadorComponent', () => {
  let component: EditarJogadorComponent;
  let fixture: ComponentFixture<EditarJogadorComponent>;

  beforeEach(() => {
    const locationStub = () => ({ back: () => ({}) });
    const editarJogadorServiceStub = () => ({
      getJogadorByEmailAsync: email => ({ subscribe: f => f({}) }),
      updateUtilizador: jogadorDto => ({ subscribe: f => f({}) })
    });
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [EditarJogadorComponent],
      providers: [
        { provide: Location, useFactory: locationStub },
        { provide: EditarJogadorService, useFactory: editarJogadorServiceStub }
      ]
    });
    fixture = TestBed.createComponent(EditarJogadorComponent);
    component = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it(`listaJogadores has default value`, () => {
    expect(component.listaJogadores).toEqual([]);
  });

  it(`uploadFileName has default value`, () => {
    expect(component.uploadFileName).toEqual(`Choose File`);
  });

  describe('ngOnInit', () => {
    it('makes expected calls', () => {
      spyOn(component, 'getJogadorByEmailAsync').and.callThrough();
      component.ngOnInit();
      expect(component.getJogadorByEmailAsync).toHaveBeenCalled();
    });
  });

  describe('back', () => {
    it('makes expected calls', () => {
      const locationStub: Location = fixture.debugElement.injector.get(
        Location
      );
      spyOn(locationStub, 'back').and.callThrough();
      component.back();
      expect(locationStub.back).toHaveBeenCalled();
    });
  });
});
