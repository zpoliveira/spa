import { HttpResponse } from '@angular/common/http';
import { InvokeFunctionExpr } from '@angular/compiler';
import { error } from '@angular/compiler/src/util';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatCard } from '@angular/material/card';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { PaginacaoDto } from 'src/app/dto/paginacaoDto';
import { EditarRelacionamentoDto } from 'src/app/dto/Relacionamento/editarRelacionamentoDto';
import { UpdateRelacionamentoDto } from 'src/app/dto/Relacionamento/updateRelacionamentoServiceDto';
import { Relacionamento } from "../../model/relacionamento";
import { RelacionamentoService } from "../../services/relacionamento/relacionamento.service";
import { EditarRelacionamentoComponent } from '../editar-relacionamento/editar-relacionamento.component';
@Component({
  selector: 'app-lista-relacionamento',
  templateUrl: './lista-relacionamento.component.html',
  styleUrls: ['./lista-relacionamento.component.scss']
})
export class ListaRelacionamentoComponent implements OnInit {

  private pageSize = 10;
  private currentPage = 0;
  private totalSize = 0;
  emailDe: string = '1080510@isep.ipp.pt';
  relacionamentos: Relacionamento[] | any[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;



  constructor(private relacionamentoService: RelacionamentoService, private dialog: MatDialog) { }

  ngOnInit(): void {


  }

  ngAfterViewInit() {
    this.paginator.showFirstLastButtons = true;
    this.paginator.pageSize = this.pageSize;
    this.paginator.length = this.totalSize;
    this.paginator.pageSizeOptions = [10, 50, 100];
    this.paginator.pageIndex = this.currentPage;
    this.paginator.page.subscribe(x => this.handlePage(x));
    //this.getRelacionamentos();
  }

  private getRelacionamentos() {
    this.relacionamentoService.getRelacionamentos(this.emailDe, this.currentPage + 1, this.pageSize)
      .subscribe(x => {
        //console.log(x.headers);
        let jsonObj: any = JSON.parse(x.headers.get("x-paginacao"));
        let paginacao: PaginacaoDto = <PaginacaoDto>jsonObj;
        this.relacionamentos = x.body;
        this.currentPage = paginacao.numeroPagina - 1;
        this.pageSize = paginacao.tamanhoPagina;
        this.totalSize = paginacao.contagemTotal;
        this.paginator.length = paginacao.contagemTotal;
        //return this.relacionamentos;
      });
  }

  lerRelacionamentos() {
    console.log(this.emailDe);
    this.getRelacionamentos();
  }

  handlePage(e: PageEvent) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getRelacionamentos();
  }

  updateRelacionamento(relacionamento: EditarRelacionamentoDto) {

    let update: UpdateRelacionamentoDto = {
      EmailJogadorPara: relacionamento.EmailJogadorPara,
      EmailJogadorDe: relacionamento.EmailJogadorDe,
      Forca: relacionamento.Forca,
      LstTags: [...relacionamento.LstTags]
    }
    console.log(update);

    this.relacionamentoService.updateRelacionamento(update)
      .subscribe(x => {
        console.log(x);
        this.getRelacionamentos();
      });
  }

  editRelacionamento(relacionamento: Relacionamento) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    let editRelacionamento: EditarRelacionamentoDto = {
      Nome: relacionamento.nome,
      EmailJogadorPara: relacionamento.emailPara,
      EmailJogadorDe: relacionamento.emailDe,
      Forca: relacionamento.forcaLigacao,
      LstTags: Object.assign([], relacionamento.tags)
    }

    dialogConfig.data = editRelacionamento

    const dialogRef = this.dialog.open(EditarRelacionamentoComponent,
      dialogConfig);

    dialogRef.componentInstance.submitClicked
      .subscribe(result => {
        console.log('Got the data!', result);
        this.updateRelacionamento(result);
        dialogRef.close();
      });


  }

}
