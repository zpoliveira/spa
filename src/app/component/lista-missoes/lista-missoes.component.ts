import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MissaoDto } from 'src/app/dto/missao/missaoDto';
import { PaginacaoDto } from 'src/app/dto/paginacaoDto';
import { MissaoService } from 'src/app/services/missao/missao.service';

@Component({
  selector: 'app-lista-missoes',
  templateUrl: './lista-missoes.component.html',
  styleUrls: ['./lista-missoes.component.scss']
})
export class ListaMissoesComponent implements OnInit {

  private pageSize = 10;
  private currentPage = 0;
  private totalSize = 0;
  emailDe: string = '1080510@isep.ipp.pt';
  missoes: MissaoDto[] | any[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;



  constructor(private missaoService: MissaoService) { }

  ngOnInit(): void {


  }

  ngAfterViewInit() {
    this.paginator.showFirstLastButtons = true;
    this.paginator.pageSize = this.pageSize;
    this.paginator.length = this.totalSize;
    this.paginator.pageSizeOptions = [10, 50, 100];
    this.paginator.pageIndex = this.currentPage;
    this.paginator.page.subscribe(x => this.handlePage(x));
    //this.getRelacionamentos();
  }

  private getMissoes() {
    this.missaoService.getMissoes(this.emailDe, this.currentPage + 1, this.pageSize)
      .subscribe(x => {
        //console.log(x.headers);
        let jsonObj: any = JSON.parse(x.headers.get("x-paginacao"));
        let paginacao: PaginacaoDto = <PaginacaoDto>jsonObj;
        this.missoes = x.body;
        this.currentPage = paginacao.numeroPagina - 1;
        this.pageSize = paginacao.tamanhoPagina;
        this.totalSize = paginacao.contagemTotal;
        this.paginator.length = paginacao.contagemTotal;
        //return this.relacionamentos;
      });
  }

  lerMissoes() {
    console.log(this.emailDe);
    this.getMissoes();
  }

  handlePage(e: PageEvent) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getMissoes();
  }

}
