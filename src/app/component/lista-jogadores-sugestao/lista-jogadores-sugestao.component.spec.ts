import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaJogadoresSugestaoComponent } from './lista-jogadores-sugestao.component';

describe('ListaJogadoresSugestaoComponent', () => {
  let component: ListaJogadoresSugestaoComponent;
  let fixture: ComponentFixture<ListaJogadoresSugestaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListaJogadoresSugestaoComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaJogadoresSugestaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
