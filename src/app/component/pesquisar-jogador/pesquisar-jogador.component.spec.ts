import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Location } from '@angular/common';
import { PesquisarJogadorService } from '../../services/pesquisar-jogador/pesquisar-jogador.service';
import { PesquisarJogadorComponent } from './pesquisar-jogador.component';

describe('PesquisarJogadorComponent', () => {
  let component: PesquisarJogadorComponent;
  let fixture: ComponentFixture<PesquisarJogadorComponent>;

  beforeEach(() => {
    const locationStub = () => ({ back: () => ({}) });
    const pesquisarJogadorServiceStub = () => ({
      pesquisarJogadores: value => ({ subscribe: f => f({}) }),
      pedidoLigacao: pedido => ({ subscribe: f => f({}) })
    });
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [PesquisarJogadorComponent],
      providers: [
        { provide: Location, useFactory: locationStub },
        {
          provide: PesquisarJogadorService,
          useFactory: pesquisarJogadorServiceStub
        }
      ]
    });
    fixture = TestBed.createComponent(PesquisarJogadorComponent);
    component = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it(`jogadores has default value`, () => {
    expect(component.jogadores).toEqual([]);
  });

  it(`displayedColumns has default value`, () => {
    expect(component.displayedColumns).toEqual([
      `nome`,
      `email`,
      `humor`,
      `facebookURL`,
      `linkedInURL`,
      `telefone`,
      `dataNascimento`,
      `nacionalidade`,
      `cidadeResidencia`
    ]);
  });

  describe('ngOnInit', () => {
    it('makes expected calls', () => {
      spyOn(component, 'initForm').and.callThrough();
      component.ngOnInit();
      expect(component.initForm).toHaveBeenCalled();
    });
  });

  describe('back', () => {
    it('makes expected calls', () => {
      const locationStub: Location = fixture.debugElement.injector.get(
        Location
      );
      spyOn(locationStub, 'back').and.callThrough();
      component.back();
      expect(locationStub.back).toHaveBeenCalled();
    });
  });
});
