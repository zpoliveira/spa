import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
// @ts-ignore
import { AuthService } from "../auth/auth.service";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private authListenerSubs: Subscription;
  userIsAuthenticated = false;

  @Output() toggleEmiter = new EventEmitter();

  constructor() { }
  // constructor(public authService: AuthService) {}

  ngOnInit(): void {
    /*  this.authListenerSubs = this.authService
        .getAuthStatusListener()
        .subscribe(isAuthenticated => {
            this.userIsAuthenticated = isAuthenticated;
        });*/
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }

  onLogout() {
    // this.authService.logout();
  }

  toggle() {
    this.toggleEmiter.emit();
  }

}
