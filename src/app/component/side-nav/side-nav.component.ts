import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}


@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  @ViewChild('drawer') drawer: MatSidenav

  MENUITEMS = [
    { state: 'dashboard', name: 'Dashboard', type: 'link', icon: 'av_timer' },
    { state: 'perfil', type: 'link', name: 'Perfil', icon: 'account_circle' },
    { state: '/editarJogador', type: 'link', name: 'Editar Perfil', icon: 'mode_edit' },
    { state: '/editarHumor', type: 'link', name: 'Editar Humor', icon: 'emoji_emotions' },
    { state: '/pesquisarJogador', type: 'link', name: 'Pesquisar Jogador', icon: 'search' },
    { state: '/pedirIntroducao', type: 'link', name: 'Pedido Introdução', icon: 'timeline' },
    { state: 'relacionamentos', type: 'link', name: 'Relacionamentos', icon: 'people_alt' },
    { state: 'missoes', type: 'link', name: 'Missões', icon: 'task_alt' },
    { state: 'pedidos', type: 'link', name: 'Pedidos', icon: 'notifications' },
    { state: 'jogadoresSugestao', type: 'link', name: 'Jogador Sugestao', icon: 'card_giftcard' },
    //{ state:'logout', type:'link', name: 'Sair', icon:'logout'}
  ];

  constructor() { }

  ngOnInit(): void {
  }

  toggle() {
    this.drawer.toggle();
  }


}
