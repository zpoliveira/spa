export interface AceitarLigacaoDto {
  idIntroducao: string,
  emailJogador: string,
  aceitar: boolean,
  forcaLigacao: number,
  tags: string[]
}
