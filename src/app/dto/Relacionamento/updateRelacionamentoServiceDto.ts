export interface UpdateRelacionamentoDto {
    EmailJogadorDe:string;
    EmailJogadorPara: string;
    Forca: number;
    LstTags:string[];
}