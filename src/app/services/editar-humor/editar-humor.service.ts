import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Humor } from '../../model/humor';
import { MessagesService } from '../messages/messages.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { EditarHumorDto } from 'src/app/dto/editarHumorDto';
import { AtualizarHumorDto } from 'src/app/dto/atualizarHumorDto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class EditarHumorService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json-patch+json' /*'application/json'*/
    })
  };


  private url = environment.urlAddress;  //ver estrutura db
  //private url = 'https://localhost:5001';
  private listaHumorUrl = '/api/Jogador/estadosHumor'
  private editarHumorUrl = '/api/Jogador/humor';



  constructor(private messagesService: MessagesService, private http: HttpClient) { }

  getEstadosHumor(): Observable<Humor[]> {
    const url = this.url + this.listaHumorUrl;
    return this.http.get<Humor[]>(url)
      .pipe(
        //tap(_ => this.log('')),
        catchError(this.handleError<Humor[]>('getEstadosHumor', []))
      );
  }

  patchEstadoHumor(editarHumor: EditarHumorDto): Observable<AtualizarHumorDto> {
    const url = this.url + this.editarHumorUrl;

    return this.http.patch<AtualizarHumorDto>(url, editarHumor, this.httpOptions)
      .pipe(
        //tap(_ => this.log(`Atualizado humor do jogador=${editarHumor.email}`)),
        tap((jogHumor: AtualizarHumorDto) => this.log(`Atualizado humor do jogador ${jogHumor.nome} para ${jogHumor.humor}`)),
        catchError(this.handleError<any>('patchEstadoHumor'))
      );
  }


  private log(message: string) {
    this.messagesService.add(`EditarHumorService: ${message}`);
  }

  //retrived from https://angular.io/tutorial/toh-pt6
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    }
  }
}
