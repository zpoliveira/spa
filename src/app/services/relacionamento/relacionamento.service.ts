import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Relacionamento } from 'src/app/model/relacionamento';
import { UpdateRelacionamentoDto } from 'src/app/dto/Relacionamento/updateRelacionamentoServiceDto';
import { ApiService } from 'src/app/services/shared/api.service';

@Injectable({
  providedIn: 'root'
})
export class RelacionamentoService {

  constructor(private api: ApiService) { }

  private uri: string = "api/relacionamento"

  getRelacionamentos(email: string, numeroPagina: number, totalPagina: number): Observable<HttpResponse<Relacionamento[]>> {
    return this.api.getDataWithHeaders(this.uri, { "email": email, "numeroPagina": numeroPagina, "totalPagina": totalPagina }) as Observable<HttpResponse<Relacionamento[]>>;

  }

  updateRelacionamento(relacionamento: UpdateRelacionamentoDto): Observable<UpdateRelacionamentoDto> {
    return this.api.update(this.uri, relacionamento) as Observable<UpdateRelacionamentoDto>;
  }
}
