import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AceitarLigacaoDto } from 'src/app/dto/aceitarLigacaoDto';
import { PedidoLigacaoDto } from 'src/app/dto/pedidoLigacaoDto';
import { LigacaoPendenteDto } from 'src/app/model/ligacaoPendente';
import { ApiService } from 'src/app/services/shared/api.service';

@Injectable({
  providedIn: 'root'
})
export class PedidosLigacaoService {

  constructor(private api: ApiService) { }

  private uriBase: string = "api/introducao"
  private uriAceitar: string = this.uriBase + "/aceitar"

  getLigacoesPendentes(email: string): Observable<LigacaoPendenteDto[]> {
    console.log("getting ligacoes");

    return this.api.getData(this.uriBase, { "jogadorRequisitanteEmail": email }) as Observable<LigacaoPendenteDto[]>;
  }

  acceptLigacao(dto: AceitarLigacaoDto) {
    return this.api.update(this.uriAceitar, dto);
  }
}
