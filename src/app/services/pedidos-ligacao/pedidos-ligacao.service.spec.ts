import { TestBed } from '@angular/core/testing';
import { AceitarLigacaoDto } from 'src/app/dto/aceitarLigacaoDto';
import { ApiService } from 'src/app/services/shared/api.service';
import { PedidosLigacaoService } from './pedidos-ligacao.service';

describe('PedidosLigacaoService', () => {
  let service: PedidosLigacaoService;

  beforeEach(() => {
    const apiServiceStub = () => ({
      getData: (uriBase, object) => ({}),
      update: (uriAceitar, dto) => ({})
    });
    TestBed.configureTestingModule({
      providers: [
        PedidosLigacaoService,
        { provide: ApiService, useFactory: apiServiceStub }
      ]
    });
    service = TestBed.inject(PedidosLigacaoService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  describe('acceptLigacao', () => {
    it('makes expected calls', () => {
      const aceitarLigacaoDtoStub: AceitarLigacaoDto = <any>{};
      const apiServiceStub: ApiService = TestBed.inject(ApiService);
      spyOn(apiServiceStub, 'update').and.callThrough();
      service.acceptLigacao(aceitarLigacaoDtoStub);
      expect(apiServiceStub.update).toHaveBeenCalled();
    });
  });
});
