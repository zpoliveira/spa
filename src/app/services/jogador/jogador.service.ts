import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JogadorSugestaoDto } from 'src/app/dto/jogador/jogadorSugestaoDto';
import { ApiService } from '../shared/api.service';

@Injectable({
  providedIn: 'root'
})
export class JogadorService {

  constructor(private api: ApiService) { }

  private uri: string = "api/Jogador"


  getSugestoes(email: string, numeroPagina: number, totalPagina: number): Observable<HttpResponse<JogadorSugestaoDto[]>> {
    return this.api.getDataWithHeaders(`${this.uri}/Sugestoes`, { "email": email, "numeroPagina": numeroPagina, "totalPagina": totalPagina }) as Observable<HttpResponse<JogadorSugestaoDto[]>>;

  }
}
