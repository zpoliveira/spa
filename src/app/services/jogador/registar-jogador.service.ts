import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class RegistarJogadorService {

  constructor(private fb: FormBuilder, private http: HttpClient) { }
  readonly BaseURI = 'https://localhost:5001/api/Jogador/';



  formModel = this.fb.group({
    nome: ['', Validators.required],
    email: ['', Validators.email],
    telefone: ['', Validators.min(9), Validators.max(9)],
    urlFacebook: [''],
    urlLinkedIn: [''],
    dataNascimentoInput: ['', Validators.required],
    cidadeResidencia: [''],
    descricaoBreve: [''],
    avatar: [''],
    tagList: [''],
    Passwords: this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required]
    }, { validator: this.comparePasswords })

  });

  comparePasswords(fb: FormGroup) {
    let confirmPswrdCtrl = fb.get('ConfirmPassword');
    //passwordMismatch
    //confirmPswrdCtrl.errors={passwordMismatch:true}
    if (confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl.errors) {
      if (fb.get('Password').value != confirmPswrdCtrl.value)
        confirmPswrdCtrl.setErrors({ passwordMismatch: true });
      else
        confirmPswrdCtrl.setErrors(null);
    }
  }

  register() {
    var body = {
      nome: this.formModel.value.nome,
      email: this.formModel.value.email,
      dataNascimento: this.formModel.value.dataNascimento,
      password: this.formModel.value.Passwords.Password,
      telefone: this.formModel.value.telefone,
      urlFacebook: this.formModel.value.urlFacebook,
      urlLinkedIn: this.formModel.value.urlLinkedIn,
    };
    return this.http.post(this.BaseURI + '/ApplicationUser/Register', body);
  }
}
