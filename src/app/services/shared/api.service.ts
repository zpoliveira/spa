import { Injectable } from '@angular/core';
import { Params } from "@angular/router";
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  public getData = (route: string, param: Params) => {
    const paramsHttp = { params: param };
    return this.http.get(this.createCompleteRoute(route, environment.urlAddress), paramsHttp);
  }

  public getDataWithHeaders = (route: string, param: Params) => {
    return this.http.get(this.createCompleteRoute(route, environment.urlAddress), this.generateHeadersGet(param));
  }

  public create = (route: string, body) => {
    return this.http.post(this.createCompleteRoute(route, environment.urlAddress), body, this.generateHeaders());
  }

  public update = (route: string, body) => {
    return this.http.put(this.createCompleteRoute(route, environment.urlAddress), body, this.generateHeaders());
  }

  public partialUpdate = (route: string, body) => {
    return this.http.patch(this.createCompleteRoute(route, environment.urlAddress), body, this.generateHeaders());
  }

  public delete = (route: string) => {
    return this.http.delete(this.createCompleteRoute(route, environment.urlAddress));
  }

  private createCompleteRoute = (route: string, envAddress: string) => {
    return `${envAddress}/${route}`;
  }


  private generateHeaders = () => {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        'Access-Control-Allow-Methods': ['GET', 'POST', 'PATCH', 'PUT', 'DELETE', 'OPTIONS'],
        'Access-Control-Allow-Headers': ['Origin', 'Content-Type', 'x-paginacao']
      })
    };
  }

  private generateHeadersGet = (param: Params) => {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        'Access-Control-Allow-Methods': ['GET', 'POST', 'PATCH', 'PUT', 'DELETE', 'OPTIONS'],
        'Access-Control-Allow-Headers': ['Origin', 'Content-Type', 'x-paginacao']
      }),
      observe: "response" as "response",
      params: param,

    };
  }
}
