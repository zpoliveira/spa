import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarJogadorComponent } from './component/editarJogador/editarJogador.component';
import { ListaRelacionamentoComponent } from './component/lista-relacionamento/lista-relacionamento.component';
import { PesquisarJogadorComponent } from './component/pesquisar-jogador/pesquisar-jogador.component';
import { SignupComponent } from "./component/signup/signup.component";
import { LoginComponent } from "./auth/login/login.component";
import { ListaJogadoresSugestaoComponent } from './component/lista-jogadores-sugestao/lista-jogadores-sugestao.component';
import { ListaMissoesComponent } from './component/lista-missoes/lista-missoes.component';
import { EditarHumorComponent } from './component/editar-humor/editar-humor.component';
import { PedirIntroducaoComponent } from './component/pedir-introducao/pedir-introducao.component';
import { ListaPedidosPendentesComponent } from './component/lista-pedidos-pendentes/lista-pedidos-pendentes.component';

const routes: Routes = [
  { path: 'editarJogador', component: EditarJogadorComponent },
  { path: 'editarHumor', component: EditarHumorComponent },
  { path: 'pesquisarJogador', component: PesquisarJogadorComponent },
  { path: 'relacionamentos', component: ListaRelacionamentoComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'pedidos', component: ListaPedidosPendentesComponent },
  { path: 'relacionamentos', component: ListaRelacionamentoComponent },
  { path: 'jogadoresSugestao', component: ListaJogadoresSugestaoComponent },
  { path: 'missoes', component: ListaMissoesComponent },
  { path: 'pedirIntroducao', component: PedirIntroducaoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
