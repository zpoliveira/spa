export interface Humor {
    estadoHumor: string;
  }

  //GET FROM DB
  export const EstadosHumor: Humor[] = [
    { estadoHumor: 'FELIZ' },
    { estadoHumor: 'TRISTE' },
    { estadoHumor: 'NEUTRO' }
  ];