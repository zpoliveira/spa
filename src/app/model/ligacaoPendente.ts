export interface LigacaoPendenteDto {
  idLigacao: string;
  emailJogador: string;
  forcaLigacao: number;
  introducao: boolean;
}
