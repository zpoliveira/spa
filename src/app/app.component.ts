import { Component, ViewChild } from '@angular/core';
import { SideNavComponent } from './component/side-nav/side-nav.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'social-net';

  @ViewChild('drawer') sideNavDrawer: SideNavComponent;


  toggle() {
    this.sideNavDrawer.toggle();
  }


}
